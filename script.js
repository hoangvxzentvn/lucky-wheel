var prizeSlice = 360 / prizes.length
var prizeOffset = Math.floor(180 / prizes.length)

const wheel = document.querySelector(".deal-wheel")
const spinner = wheel.querySelector(".spinner")
const trigger = wheel.querySelector(".btn-spin")
const ticker = wheel.querySelector(".ticker")
const spinClass = "is-spinning"
const selectedClass = "selected"
const spinnerStyles = window.getComputedStyle(spinner)
let tickerAnim
let rotation = 0
let currentSlice = 0
let prizeNodes

const createPrizeNodes = () => {
  prizes.forEach(({text}, i) => {
    const rotation = prizeSlice * i * -1 - prizeOffset

    spinner.insertAdjacentHTML(
      "beforeend",
      `<li class="prize" style="--rotate: ${rotation}deg">
          <span class="text">${text}</span>
        </li>`
    )
  })
}

const createConicGradient = () => {
  spinner.setAttribute(
    "style",
    `background: conic-gradient(
        from -90deg,
        ${prizes
          .map(
            (_, i) =>
              `${colors[i % colors.length]} 0 ${
                (100 / prizes.length) * (prizes.length - i)
              }%`
          )
          .reverse()}
      );`
  )
}

const setupWheel = () => {
  createConicGradient()
  createPrizeNodes()
  prizeNodes = wheel.querySelectorAll(".prize")
}

const spinertia = (min, max) => {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1)) + min
}

const runTickerAnimation = () => {
  const values = spinnerStyles.transform.split("(")[1].split(")")[0].split(",")
  const a = values[0]
  const b = values[1]
  let rad = Math.atan2(b, a)

  if (rad < 0) rad += 2 * Math.PI

  const angle = Math.round(rad * (180 / Math.PI))
  const slice = Math.floor(angle / prizeSlice)

  if (currentSlice !== slice) {
    ticker.style.animation = "none"
    setTimeout(() => (ticker.style.animation = null), 10)
    currentSlice = slice
  }

  tickerAnim = requestAnimationFrame(runTickerAnimation)
}

const selectPrize = () => {
  const selected = Math.floor(rotation / prizeSlice)
  prizeNodes[selected].classList.add(selectedClass)
}

const getRandomElementByRate = (array) => {
  const totalRate = array.reduce((sum, element) => sum + element.rate || 0, 0)
  const randomNumber = Math.random() * totalRate
  let cumulativeRate = 0

  for (const [index, element] of array.entries()) {
    cumulativeRate += element.rate

    if (randomNumber <= cumulativeRate) {
      return [index, element]
    }
  }

  return [array.length - 1, array[array.length - 1]]
}

const randomDEG = () => {
  const [index] = getRandomElementByRate(prizes)
  const result =
    (index / prizes.length) * 360 +
    360 * spinertia(7, 10) +
    spinertia(1, 360 / prizes.length - 1)
  return Math.floor(result)
}

trigger.addEventListener("click", () => {
  trigger.disabled = true
  rotation = randomDEG()
  prizeNodes.forEach((prize) => prize.classList.remove(selectedClass))
  wheel.classList.add(spinClass)
  spinner.style.setProperty("--rotate", rotation)
  ticker.style.animation = "none"
  runTickerAnimation()
})

spinner.addEventListener("transitionend", () => {
  cancelAnimationFrame(tickerAnim)
  trigger.disabled = false
  trigger.focus()
  rotation %= 360
  selectPrize()
  wheel.classList.remove(spinClass)
  spinner.style.setProperty("--rotate", rotation)
})

setupWheel()

// for (let i = 0; i < 10; i++) {
//   const test = {}
//   for (let j = 0; j < 100; j++) {
//     const [_, ele] = getRandomElementByRate(prizes)
//     if (ele.text in test) {
//       test[ele.text] += 1
//     } else {
//       test[ele.text] = 1
//     }
//   }
//   const test2 = []
//   for (const prize of prizes) {
//     test2.push([prize.rate, prize.text, test[prize.text]])
//   }
//   console.log(test2)
// }
