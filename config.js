var prizes = [
  {
    text: "10% Off Sticker Price",
    rate: 5,
  },
  {
    text: "Half Off Sticker Price",
    rate: 10,
  },
  {
    text: "Free DIY Carwash",
    rate: 10,
  },
  {
    text: "Free Car",
    rate: 5,
  },
  {
    text: "No Money Down",
    rate: 5,
  },
  {
    text: "Eternal Damnation",
    rate: 10,
  },
  {
    text: "One Solid Hug",
    rate: 45,
  },
  {
    text: "Used Travel Mug",
    rate: 10,
  },
]
var colors = [
  "hsl(197 30% 43%)",
  "hsl(173 58% 39%)",
  "hsl(43 74% 66%)",
  "hsl(27 87% 67%)",
  "hsl(12 76% 61%)",
  "hsl(350 60% 52%)",
  "hsl(91 43% 54%)",
  "hsl(140 36% 74%)",
]
